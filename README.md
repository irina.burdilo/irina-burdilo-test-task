# irina-burdilo-cba-task

This project cover API test for following:
https://supervillain.herokuapp.com/api-docs/

# To run Test:
```
mvn clean test
```

# To generate Allure Report

Allure results will be generated in to the build directort target/allure-results 
Run following to generate allure report from results 

```
mvn allure:serve
```

# Config

config.properties file can control the browser and driver location. 
Please add corresponding drivers to the  src/test/resources/driver/ folder

Current test support Chrome and Firefox browsers
