package cba.task.test;

import cba.task.test.api.RestAssuredClient;
import cba.task.test.api.SupervillianHelper;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class SupervillanApiTest {
    SupervillianHelper supervillianHelper;

    @BeforeAll
    public static void setUp() throws IOException {
        CommonHelper.loadProperties();

    }

    @Test
    public void userFlowTest() throws IOException {
        supervillianHelper = new SupervillianHelper();
        //create new user
        Response response = supervillianHelper.addUser();
        Assertions.assertEquals(response.getStatusCode(), 201);
        String createdUser = supervillianHelper.createdUser;

        //get list of users and validate new user is there
        response = supervillianHelper.getAllUsers();
        Assertions.assertTrue(response.getStatusCode() == 200);
        List<Map> userList = RestAssuredClient.responseToObject(response, List.class);
        userList.stream().filter(user -> user.get("username").equals(createdUser)).findFirst().orElse(null);

        //update user
        response = supervillianHelper.updateUser("5");
        Assertions.assertEquals(204, response.getStatusCode());

        //get user and validated updated score value
        response = supervillianHelper.getAllUsers();
        Assertions.assertTrue(response.getStatusCode() == 200);
        userList = RestAssuredClient.responseToObject(response, List.class);
        Map updatedUser =  userList.stream().filter(user -> user.get("username").equals(createdUser)).findFirst().orElse(null);
        Assertions.assertEquals(updatedUser.get("score"), 5);

        //delete user
        ///ISSUE: As part of delete we should also pass id of the user to delete
        String deleteKey = "delete key should be provided";
        response = supervillianHelper.deleteUser(deleteKey);
        Assertions.assertEquals(response.getStatusCode(), 200);
    }
}
