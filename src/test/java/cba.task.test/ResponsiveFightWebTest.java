package cba.task.test;

import cba.task.test.web.BaseSelenium;
import cba.task.test.web.ResponsiveFightPage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class ResponsiveFightWebTest {

    static WebDriver driver;
    static ResponsiveFightPage fightPage;

    @BeforeAll
    public static void setUp() throws IOException {
        CommonHelper.loadProperties();
        driver = new BaseSelenium((String) CommonHelper.properties.get("browserName")).driver;
        fightPage = new ResponsiveFightPage(driver);
        driver.manage().window().maximize();
    }

    @AfterAll
    static void tearDown() {
        driver.quit();
    }

    @Test
    public void playBusGameTest() throws IOException {
        fightPage.openGame();
        fightPage.createUser("testUser" + CommonHelper.getTimeStamp());
        fightPage.chooseGame();
        fightPage.selectBusGame();
        fightPage.selectFirstAnswer();
        fightPage.selectNext();
    }
}
