package cba.task.test.web;

import cba.task.test.CommonHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class ResponsiveFightPage {
    WebDriver driver;

    public ResponsiveFightPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "worrior_username")
    public WebElement txtUsername;

    @FindBy(id = "warrior")
    public WebElement btnCreate;

    @FindBy(id = "start")
    public WebElement btnStart;

    @FindBy(id = "welcome_text")
    public WebElement lnkWelcome;

    @FindBy(id = "bus")
    public WebElement lnkBus;

    @FindBy(id = "bus_timer_start")
    public WebElement btnBusStart;

    @FindBy(id = "bus_title")
    public WebElement headerBus;

    @FindBy(id = "bus_answer_1")
    public WebElement busAnswer1;

    @FindBy(id = "close_correct_modal_btn")
    public WebElement btnCorrectNext;

    @Step("Open Game")
    public void openGame() {
        driver.get(CommonHelper.properties.get("responsivefightUrl").toString());
    }

    @Step("Create user")
    public void createUser(String userName) throws IOException {
        txtUsername.sendKeys(userName);
        btnCreate.click();
        waitForTextInElement(btnStart, userName);
    }

    @Step("Choose Game")
    public void chooseGame() throws IOException {
        btnStart.click();
        waitForTextInElement(lnkWelcome, "Choose your battle field");
    }

    @Step("Select Bus Game")
    public void selectBusGame() throws IOException {
        moveToElement(lnkBus);
        lnkBus.click();
        wait(btnBusStart);
        btnBusStart.click();
        isEnabled(headerBus);
        waitForTextInElement(headerBus, "Inside the Bus");

    }

    @Step("Select First Answer")
    public void selectFirstAnswer() throws IOException {
        busAnswer1.click();
        waitForTextInElement(busAnswer1, "correct");
    }

    @Step("Next Question")
    public void selectNext() throws IOException {
        isEnabled(btnCorrectNext);
        btnCorrectNext.click();
    }

    public boolean wait(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.visibilityOf(element));

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean waitForTextInElement(WebElement element, String text) throws IOException {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.textToBePresentInElement(element, text));
        } catch (Exception e) {

            return false;
        }
        return true;
    }

    public boolean isEnabled(WebElement element) throws IOException {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 60);
            wait.until(ExpectedConditions.elementToBeClickable(element));

        } catch (Exception e) {

            return false;
        }
        return true;
    }

    public void moveToElement(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }
}
