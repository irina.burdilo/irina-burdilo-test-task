package cba.task.test.web;

import cba.task.test.CommonHelper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseSelenium {
    public WebDriver driver;
    private static String OS = (String) CommonHelper.properties.get("osName");

    public BaseSelenium(String browser){
        driver=openBrowser(browser);
    }


    public WebDriver openBrowser(String browser){
        switch (browser.toUpperCase()) {
            case "FIREFOX":
                openInFirefox();
                break;
            case "CHROME":
                openInChrome();
                break;
        }
        return driver;
    }

    public WebDriver openInFirefox(){
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        String path="";
        if (!OS.contains("windows")) {
            path = BaseSelenium.class.getClassLoader().getResource(CommonHelper.properties.get("firefoxDriver").toString()).getFile();
        }
        else {
            path = path + ".exe";

        }
        System.setProperty("webdriver.gecko.driver", path);
        driver = new FirefoxDriver(firefoxOptions);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return driver;
    }

    public WebDriver openInChrome(){
        ChromeOptions options = new ChromeOptions();
        String path = "";
        if (!OS.contains("windows")) {
            path = BaseSelenium.class.getClassLoader().getResource(CommonHelper.properties.get("chromeDriver").toString()).getFile();
        }
        else {
            path = path + ".exe";

        }
        System.setProperty("webdriver.chrome.driver", path);
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return driver;
    }

}
