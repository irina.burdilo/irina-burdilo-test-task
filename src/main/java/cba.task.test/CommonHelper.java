package cba.task.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.*;
import java.net.URL;
import java.nio.file.FileSystem;
import java.util.Map;
import java.util.Properties;

public final class CommonHelper {
    public static Properties properties;
    private static String PROPERTIES_FILE="config.properties";
    private static String PAYLOAD_FILE="testData/add-user.json";
    private static ObjectMapper objectMapper = new ObjectMapper();

    private CommonHelper(){

    }
    public static void loadProperties() throws IOException {
        properties = new Properties();
        String file=  CommonHelper.class.getClassLoader().getResource(PROPERTIES_FILE).getFile();
        File propsFile = new File(String.valueOf(file));
        properties.load(new FileInputStream(propsFile));

    }

    public static Map loadJsonFile() throws IOException {
        String file=  CommonHelper.class.getClassLoader().getResource(PAYLOAD_FILE).getFile();
        InputStream fileStream = new FileInputStream(file);
        return objectMapper.readValue(fileStream, Map.class);
    }

    public static String getTimeStamp(){
        return String.valueOf(System.currentTimeMillis());
    }
}
