package cba.task.test.api;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.qameta.allure.Step;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class RestAssuredClient {
    private RequestSpecification httpRequest;

    public RestAssuredClient() {

    }

    @Step("Set base URL")
    public RestAssuredClient setBaseURI(String baseURI) {
        RestAssured.baseURI = baseURI;
        this.httpRequest = given().urlEncodingEnabled(false).log().all();
        return this;
    }

    @Step("Set Content Type")
    public RestAssuredClient setContentType(ContentType type) {
        httpRequest.contentType(type);
        return this;
    }

    @Step("Set request header")
    public RestAssuredClient setHeader(String name, String value) {
        httpRequest.header(name, value);
        return this;
    }

    @Step("Set body of the request")
    public RestAssuredClient setBody(Object payload) {
        httpRequest.body(payload);
        return this;
    }

    @Step("Get Request")
    public Response getResponse(String path) {
        return httpRequest.get(path);
    }

    @Step("Post Request with payload")
    public Response postRequest(String path, Map payload) {
        setBody(payload);
        return httpRequest.post(path);
    }

    @Step("Put Request with body")
    public Response putRequest(String path, Map payload) {
        setBody(payload);
        return httpRequest.put(path);
    }

    @Step("Delete Request")
    public Response deleteRequest(String path) {
        return httpRequest.delete(path);
    }


    @Step("Map response to generic Object")
    public static <T> T responseToObject(Response response, Class<T> responseType) {
        return response.getBody().as(responseType);
    }
}