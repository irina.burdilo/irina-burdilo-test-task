package cba.task.test.api;

import cba.task.test.CommonHelper;
import com.fasterxml.jackson.databind.JsonNode;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.Map;

import static cba.task.test.CommonHelper.getTimeStamp;
public class SupervillianHelper {
    public String createdUser;
    RestAssuredClient client;

    public SupervillianHelper() {
        client = new RestAssuredClient();
        client.setBaseURI((String) CommonHelper.properties.get("supervillianUrl")).setContentType(ContentType.JSON);
    }

    @Step(" Get all users")
    public Response getAllUsers() {
        Response response = client.getResponse("/v1/user");
        return response;
    }

    @Step("Create user")
    public Response addUser() throws IOException {
        Map payload=CommonHelper.loadJsonFile();
        createdUser=payload.get("username")+getTimeStamp();
        payload.put("username",createdUser);
        Response response = client.postRequest("/v1/user", payload);
        return response;
    }


    @Step("Update user")
    public Response updateUser(String updatedScore) throws IOException {
        Map payload=CommonHelper.loadJsonFile();
        payload.put("username",createdUser);
        payload.put("score", updatedScore);
        Response response = client.putRequest("/v1/user", payload);
        return response;
    }

    @Step("Delete user")
    public Response deleteUser(String deleteKey) {
        client.setHeader("delete-key", deleteKey);
        Response response = client.deleteRequest("/v1/user");
        return response;
    }
}

